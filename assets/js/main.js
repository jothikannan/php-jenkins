$(document).ready(function($) {

    "use strict";

    [].slice.call(document.querySelectorAll('select.cs-select')).forEach(function(el) {
        new SelectFx(el);
    });

    jQuery('.selectpicker').selectpicker;
	
	$('.carousel').carousel({});

    if ($('#datetimepicker').length > 0) {
       $('#datetimepicker').datepicker({
		endDate: "+0d",
		orientation: "top auto",
		autoclose: true,
		
	});
    }
	
	 if ($('.table.table-striped').length > 0) {
        $('.table.table-striped').floatThead({
            position: 'fixed'
        });
    }

    jQuery.validator.addMethod("validate_email", function(value, element) {
        if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
            return true;
        } else {
            return false;
        }
    }, "Please enter a valid Email.");

    jQuery.validator.addMethod("notEqual", function(value, element, param) {
        return this.optional(element) || value != $(param).val();
    }, "This has to be different pond.");


    $.validator.addMethod("chkduplicate", function(value, element, arg) {
        console.log(value);
        var pms = JSON.parse(arg);
        pms["search"] = value;
        var rkey = $(element).closest("form").find("input[name=rkey]").val() || "";
        pms["rkey"] = rkey;
        var chk_url = site_url + "common/check_duplicate";
        var flg = false;
        $.ajax({
            type: "GET",
            data: pms,
            url: chk_url,
            async: false,
        }).done(function(resp) {
            var rdata = JSON.parse(resp);
            console.log(resp);
            console.log(rdata.status);
            if (rdata.status == "success") {
                console.log("success");
                flg = true;
            } else {
                console.log("fail");
                flg = false;
            }

        }).fail(function(err) {
            console.log(err);
        });
        console.log("123>>>>>>");
        //console.log(pms);
        return flg;
    }, "This field value already exists.");
	

    var baseUrl = $('#base').val();
	
    $('#menuToggle').on('click', function(event) {
        $('body').toggleClass('open');
    });

    $('.search-trigger').on('click', function(event) {
        event.preventDefault();
        event.stopPropagation();
        $('.search-trigger').parent('.header-left').addClass('open');
    });

    $('.search-close').on('click', function(event) {
        event.preventDefault();
        event.stopPropagation();
        $('.search-trigger').parent('.header-left').removeClass('open');
    });

    $("#login-form").validate({
		onkeyup: false,
	   	onclick: false,
	   	onfocusout: false,
        rules: {
            username: {
                required: true,
                validate_email: true
            },
            password: {
                required: true,
            },

        },
        messages: {

            username: {
                required: "Please enter a username",
                email: "Please enter a valid email address",
            },
            password: {
                required: "Please provide a password",
            },
        }
    });

    $("#stock-form").validate({
		onkeyup: false,
	   	onclick: false,
	   	onfocusout: false,
        rules: {
            pondname: {
                required: true,
            },
            count: {
                required: true,
				digits: true,
				min: 1
            },
            species_type: {
                required: true,
            },
			action_type: {
				required: true,
			},
            weight: {
                required: true,
                number: true,
                min: 0
            },
            water_type: {
                required: true,
            },
            date: {
                required: true
            }
        },
        messages: {

            pondname: {
                required: disp_text("err_pname"),
            },
            count: {
                required: disp_text("err_count"),
                number: disp_text("err_num"),
				digits: disp_text("err_dig"),
                min: disp_text("err_gt0")
            },
            species_type: {
                required: disp_text("err_stype"),
            },
            weight: {
                required: disp_text("err_weight"),
                number: disp_text("err_num"),
                min: disp_text("err_gt0")
            },
            water_type: {
                required: disp_text("err_water_type"),
            },
            date: {
                required: disp_text("err_date"),
            }
        },
        errorPlacement: function(error, element) {

            if (element.attr("name") == "date")
                error.insertAfter(".datetimepicker-icon");
            else
                error.insertAfter(element);
        }

    });

    $("#distribution-form").validate({
		onkeyup: false,
	   	onclick: false,
	   	onfocusout: false,
        rules: {
            from_pond: {
                required: true,
            },
            to_pond: {
                required: true,
                notEqual: "#from_pond"
            },
            species_type: {
                required: true,
            },
            count: {
                required: true,
				digits: true,
				min: 1
            },
            updatedby: {
                required: true,
            },
            date: {
                required: true
            }
        },
        messages: {

            from_pond: {
                required: disp_text("err_fpond"),
            },
            to_pond: {
                required: disp_text("err_topond"),
            },
            species_type: {
                required: disp_text("err_stype"),
            },
            count: {
                required: disp_text("err_count"),
                number: disp_text("err_num"),
                min: disp_text("err_gt0")
            },
            updatedby: {
                required: disp_text("err_updatedby"),
            },
            date: {
                required: disp_text("err_date"),
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "date")
                error.insertAfter(".datetimepicker-icon");
            else
                error.insertAfter(element);
        }

    });

    $("#harvest-form").validate({
		onkeyup: false,
	   	onclick: false,
	   	onfocusout: false,
        rules: {
            pondname: {
                required: true,
            },
            species_type: {
                required: true,
            },
            weight: {
                required: true,
                number: true,
                min: 0
            },
            count: {
                required: true,
				digits: true,
				min: 1
            },
            date: {
                required: true
            }
        },
        messages: {

            pondname: {
                required: disp_text("err_pname"),
            },
            species_type: {
                required: disp_text("err_stype"),
            },
            weight: {
                required: disp_text("err_weight"),
                number: disp_text("err_num"),
                min: disp_text("err_gt0")
            },
            count: {
                required: disp_text("err_count"),
                number: disp_text("err_num"),
                min: disp_text("err_gt0")
            },
            date: {
                required: disp_text("err_date"),
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "date")
                error.insertAfter(".datetimepicker-icon");
            else
                error.insertAfter(element);
        }

    });

    $("#mortality-form").validate({
		onkeyup: false,
	   	onclick: false,
	   	onfocusout: false,
        rules: {
            pondname: {
                required: true,
            },
            species_type: {
                required: true,
            },
            weight: {
                required: true,
                number: true,
                min: 0
            },
            count: {
                required: true,
				digits: true,
				min: 1
            },
            reason: {
                required: true,
            },
            date: {
                required: true
            }
        },
        messages: {

            pondname: {
                required: disp_text("err_pname"),
            },
            species_type: {
                required: disp_text("err_stype"),
            },
            weight: {
                required: disp_text("err_weight"),
                number: disp_text("err_num"),
                min: disp_text("err_gt0")
            },
            count: {
                required: disp_text("err_count"),
                number: disp_text("err_num"),
                min: disp_text("err_gt0")
            },
            reason: {
                required: disp_text("err_reason"),
            },
            date: {
                required: disp_text("err_date"),
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "date")
                error.insertAfter(".datetimepicker-icon");
            else
                error.insertAfter(element);
        }

    });
	
	
    $("#feedstock-form").validate({
		onkeyup: false,
	   	onclick: false,
	   	onfocusout: false,
        rules: {
            feed_name: {
                required: true,
            },
            stocked_weight: {
                required: true,
                number: true,
            },
            date: {
                required: true
            }
        },
        messages: {

            feed_name: {
                required: disp_text("err_fname"),
            },
            stocked_weight: {
                required: disp_text("err_sweight"),
                number: disp_text("err_num"),
            },
            date: {
                required: disp_text("err_date"),
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "date")
                error.insertAfter(".datetimepicker-icon");
            else
                error.insertAfter(element);
        }

    });	

    $("#feeddist-form").validate({
		onkeyup: false,
	   	onclick: false,
	   	onfocusout: false,
        rules: {
			pond_id: {
                required: true,
            },
            feed_stock_id: {
                required: true,
            },
            weight: {
                required: true,
                number: true,
                min: 0
            },
            date: {
                required: true
            }
        },
        messages: {
			 pond_id: {
                required: disp_text("err_pname"),
            },
            feed_stock_id: {
                required: disp_text("err_fname"),
            },
            weight: {
                required: disp_text("err_weight"),
                number: disp_text("err_num"),
                min: disp_text("err_gt0")
            },
            date: {
                required: disp_text("err_date"),
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "date")
                error.insertAfter(".datetimepicker-icon");
            else
                error.insertAfter(element);
        }

    });
	
	$("#species-form").validate({
			onkeyup: false,
			onclick: false,
			onfocusout: false,
			rules: {
				feedname: {
					required: true,
				},
				speciestype:{
					required: true,
					chkduplicate: '{ "table_name": "species", "fld_name": "species_type"}'
				},
				maxweight:{
					required: true,
					number: true,
					min :0
				}
			},
			messages: {
				feedname: {
					required:  disp_text("err_fname"),
				},
				speciestype: {
					required:  disp_text("err_stype"),
					chkduplicate:  disp_text("err_stype_exist")
				},
				maxweight: {
					required:  disp_text("err_mark_weight"),
					number : disp_text("err_num"),
					min : disp_text("err_gt0")
				}
			}
		});
	
	  $("#feed-form").validate({
			onkeyup: false,
			onclick: false,
			onfocusout: false,
			rules: {
				feedname: {
					required: true,
					chkduplicate: '{ "table_name": "feeds", "fld_name": "feedname"}',
				},
				feed_type:{
					required: true
				},
				size:{
					required: true,
					number: true,
					min:0
				}
			},
			messages: {
				
				feedname: {
					required: disp_text("err_fname"),
					chkduplicate: disp_text("err_fname_exists")
				},
				feed_type: {
					required: disp_text("err_ftype")
				},
				size: {
					required: disp_text("err_fsize"),
					number : disp_text("err_num"),
					min : disp_text("err_gt0")
				}
			}
		});
		
		
	$("#samplinglist-form").validate({
	  	onkeyup: false,
	   	onclick: false,
	   	onfocusout: false,
		rules: {
			pondname: {
				required: true,
				
			},
			species_type:{
				required: true
			},
			count:{
				required: true,
				digits: true,
				min: 1
			},
			fish_length:{
				required: true,
				number: true,
				min:0
			},
			avg_weight:{
				required: true,
				number: true,
				min:0
			},
			sampling_date:{
				required: true,
			}
			
		},
		messages: {
			
			count: {
				required: disp_text("err_count"),
				number : disp_text("err_num"),
				min : disp_text("err_gt0")
			},
			fish_length: {
				required: disp_text("err_flenth"),
				number : disp_text("err_num"),
				min : disp_text("err_gt0")
			},
			avg_weight: {
				required: disp_text("err_avgweight"),
				number : disp_text("err_num"),
				min : disp_text("err_gt0")
			},
			sampling_date: {
				required: disp_text("err_date"),
				number : disp_text("err_num"),
				min : disp_text("err_gt0")
			}
			
		},
		errorPlacement: function(error, element) {
		
		if (element.attr("name") == "sampling_date" )
			error.insertAfter(".datetimepicker-icon");
		else
			error.insertAfter(element);
		}
	});
	
	//Validate pondlist
	$("#pondlist-form").validate({
		onkeyup: false,
	   	onclick: false,
	   	onfocusout: false,
		rules: {
			pondname: {
				required: true,
				chkduplicate: '{ "table_name": "ponds", "fld_name": "pondname"}'
			},
			width: {
				required: true,
				number: true,
				min:0
			},
			length: {
				required: true,
				number: true,
				min:0
			},
			depth: {
				required: true,
				number: true,
				min:0
			},
			built_date: {
				required: true,
			},
		},
		messages: {

			pondname: {
				required: disp_text("err_pname"),
				chkduplicate: disp_text("err_pname_exists")
			},
			width: {
				required: disp_text("err_pwidth"),
				number : disp_text("err_vwidth"),
				min : disp_text("err_gt0")
			},
			length: {
				required: disp_text("err_plength"),
				number : disp_text("err_vlength"),
				min : disp_text("err_gt0")
			},
			depth: {
				required: disp_text("err_pdepth"),
				number : disp_text("err_pdepth"),
				min : disp_text("err_gt0")
			},
			built_date: {
				required: disp_text("err_date"),
			}
	},
	errorPlacement: function(error, element) {
		
		if (element.attr("name") == "built_date" )
			error.insertAfter(".datetimepicker-icon");
		else
			error.insertAfter(element);
		}
	});
	
	$("#pondcleaning-form").validate({
		onkeyup: false,
	   	onclick: false,
	   	onfocusout: false,
		rules: {
			pondname: {
				required: true,
			},
			cleanedby: {
				required: true,
			},
			cleaned_date: {
				required: true,
			},
		},
		messages: {

			pondname: {
				required: disp_text("err_psname"),
			},
			cleanedby: {
				required: disp_text("err_cleanedby"),
			},
			cleaned_date: {
				required: disp_text("err_date"),
			},
	},
	errorPlacement: function(error, element) {
		
		if (element.attr("name") == "cleaned_date" )
			error.insertAfter(".datetimepicker-icon");
		else
			error.insertAfter(element);
		}

	});	
	


    // edit ajax calls
    $('.edit-stock').on('click', function() {
		$(".btn").focus();
        var rid = $(this).data('id');
        var card = $('#stockform-card .card-body');
        card.before('<div class="ajax-spinner" style="display:none;"><img src="' + baseUrl + '/assets/images/fish-loader.gif" /></div>');
        var height = card.height();
        var width = card.width();
        card.css('opacity', '0.1');
        //$('.ajax-spinner').css({'height': height,'width':width});
        $('.ajax-spinner').show();
        $.ajax({
            type: "POST",
            url: baseUrl+"stock/editStock",
            data: {
                'rid': rid
            },
        }).done(function(data) {
            $('#stockform-card').empty();
            $('#stockform-card').html(data);
            $('.ajax-spinner').hide();
            $('#stockform-card .card-body').css('opacity', '1');
        });
    });

    $('#pondlist-table .edit-pond').on('click', function() {
		$(".btn").focus();
        var rid = $(this).data('id');
        var card = $('#pondform-card .card-body');
        card.before('<div class="ajax-spinner" style="display:none;"><img src="' + baseUrl + '/assets/images/fish-loader.gif" /></div>');
        var height = card.height();
        var width = card.width();
        card.css('opacity', '0.1');
        //$('.ajax-spinner').css({'height': height,'width':width});
        $('.ajax-spinner').show();
        $.ajax({
            type: "POST",
            url: baseUrl+"pondlist/editPond",
            data: {
                'rid': rid
            },
        }).done(function(data) {
            $('#pondform-card').empty();
            $('#pondform-card').html(data);
            $('.ajax-spinner').hide();
            $('#pondform-card .card-body').css('opacity', '1');
        });
    });
	
	$('#feeddist-form #feed_stock_id').on('change', function(){
		var rid = $(this).val();
		console.log(rid);
		if(rid){
		  $.ajax({
			  type: "POST",
			  url: baseUrl+"feed/getcurrentFeed",
			  data: {
				  'rid': rid
			  },
		  }).done(function(data) {
			  var returnedData = JSON.parse(data);
			  $('#available_weight').val(parseInt(returnedData.result_set.current_weight));
			  $('.available_weight').text("Available weight is(" + returnedData.result_set.current_weight+" kg)");
			  if(returnedData.result_set.current_weight!="undefined")
			  checkFeedStock();
		  });		
		}else{
			$('.weight_exceed_err').hide();
			$('.available_weight').text('');
		}
	});
	

    var ctx = document.getElementById("doughutChart");
    if (ctx) {

        //doughut chart
        $.ajax({
            type: "POST",
            url: baseUrl+"dashboard/getCurrentStock",
            dataType: "json",
        }).done(function(data) {
            var ponds = [];
            var fish_count = [];
            var species = [];
            var coloR = [];

            var dynamicColors = function() {
                var r = Math.floor(Math.random() * 255);
                var g = Math.floor(Math.random() * 255);
                var b = Math.floor(Math.random() * 255);
                return "rgb(" + r + "," + g + "," + b + ")";
            };
            for (var i in data.result_set) {
                ponds.push(data.result_set[i]._source.pondname + " ( " + data.result_set[i]._source.species_type + " )");
                fish_count.push(data.result_set[i]._source.count);
                species.push(data.result_set[i]._source.species_type);
                coloR.push(dynamicColors());
            }

            ctx.height = 150;
            var myChart = new Chart(ctx, {
                type: 'doughnut',
                data: {
                    datasets: [{
                        data: fish_count,
                        backgroundColor: coloR,
                        hoverBackgroundColor: coloR

                    }],
                    labels: ponds
                },
                options: {
                    responsive: true
                }
            });
        });


    }
	

	$("#loadmore-activity").click(function(e){
		e.preventDefault();
		var page = $(this).data('val');
		getActivity(page);
	});
	
	var getActivity = function(page){
	 var card = $('#activity-feed-card .card-body');
        $('#activity-feed').after('<div class="ajax-spinner-more offset-md-6" style="display:none;"><img src="' + baseUrl + '/assets/images/fish-loader.gif" /></div>');
		 $('.ajax-spinner-more').show();
	$.ajax({
		url:baseUrl+"Common/getActivities",
		type:'GET',
		data: {page:page}
		}).done(function(response){
			//console.log(response);
			if(response){
				$("#activity-feed").append(response);
				$('.ajax-spinner-more').remove();
				$('#nomore').remove();
				$('#loadmore-activity').data('val', ($('#loadmore-activity').data('val')+1));
				scroll();
			}else{
				$('.ajax-spinner-more').remove();
				$('#nomore').remove();
				$("#activity-feed").append('<div id="nomore" class="alert alert-info col-md-6 offset-md-3" role="alert"> No more data found </div>');
			}
		});
	};
	
	var scroll  = function(){
	$('html, body').animate({
		scrollTop: $('#loadmore-activity').offset().top
	}, 1000);
	};
	
	$('#stock-form #pondname').on('change', function(){
		var pond = $('#stock-form #pondname').val();
		var species_type = $('#stock-form #species_type').val();
		
		console.log(species_type);
		if(pond){
		  $.ajax({
			  type: "POST",
			  url: baseUrl+"Common/get_Currentstock",
			  data: {
				  'pond': pond,
				  
			  },
		  }).done(function(data) {		
			var response=  JSON.parse(data);
			console.log(response);
			if(response._source && response._source.pondname && response._source.count>0){
				//$('#stock-form .hide_block').hide();
				$('#species_type').val(response._source.species_id);
				$('#species_type').prop('disabled', 'disabled');
				$('#species_type').after('<input type="hidden" id="species_type_hidden" name="species_type" value="'+response._source.species_id+'"/>');
			}else {
				//$('#stock-form .hide_block').show();
				$('#species_type').prop('disabled', false);
				$('#species_type_hidden').remove();
				$('#species_type').val("");
			}
		  });
		}else{
			$('#species_type').val("");
			$('#species_type').removeAttr('disabled');
		}
	});
	
		//edit ajax calls
	$('#pondcleaning-table .edit-pond').on('click', function(){
		$(".btn").focus();
  		var rid = $(this).data('id');
		var card = $('#pondcleaningform-card .card-body');
		card.before('<div class="ajax-spinner" style="display:none;"><img src="'+baseUrl+'/assets/images/fish-loader.gif" /></div>');
		var length=card.height();
		var width=card.width();
		card.css('opacity','0.1');		
		//$('.ajax-spinner').css({'height': height,'width':width});
		$('.ajax-spinner').show();
		$.ajax({
			type:"POST",
			url: baseUrl+"pondcleaning/editPondcleaning",
			data: { 'rid' : rid },
		  }).done(function(data) {
			 $('#pondcleaningform-card').empty();
			 $('#pondcleaningform-card').html(data);
			 $('.ajax-spinner').hide();
			 $('#pondcleaningform-card .card-body').css('opacity','1');
		  });
	});
	
	//mortality count validation
	$('#mortality-form #pondname').on('change', function(){
		var pond_id = $(this).val();
		console.log(pond_id);
		if(pond_id){
		  $.ajax({
			  type: "POST",
			  url: baseUrl+"mortality/getcurrentStock",
			  data: {
				  'pond_id': pond_id
			  },
		  }).done(function(data) {
			  var returnedData = JSON.parse(data);
   			  console.log(returnedData);			  
			  var count = returnedData.result_set[0]._source.count;
			  var species_id = returnedData.result_set[0]._source.species_id;
				$('#species_type').val(species_id);
				$('#species_type').prop('disabled', 'disabled');
				$('#species_type').after('<input type="hidden" id="species_type_hidden" name="species_type" value="'+species_id+'"/>');
			  $('#available_count').val(parseInt(count));
			  $('.available_count').text("Available count is ( " + count+")");
  			  if(count!="undefined")
			  checkFishCount_mortality();
		  });		
		}else{
			$('.count_exceed_err').hide();
			$('.available_count').text('');
			$('#species_type').val("");
			$('#species_type').removeAttr('disabled');
		}
	});
	
		//edit ajax call
	$('.edit-feed').on('click', function(){
		$(".btn").focus();
		var rid = $(this).data('id');
		var card = $('#feedform-card .card-body');
		card.before('<div class="ajax-spinner" style="display:none;"><img src="'+baseUrl+'/assets/images/fish-loader.gif" /></div>');
		var height=card.height();
		var width=card.width();
		card.css('opacity','0.1');		
		$('.ajax-spinner').show();
		$.ajax({
			type:"POST",
			url:baseUrl+"feed/editFeed",
			data: { 'rid' : rid },
		  }).done(function(data) {
			 $('#feedform-card').empty();
			 $('#feedform-card').html(data);
			 $('.ajax-spinner').hide();
			 $('#feedform-card .card-body').css('opacity','1');
		  });
	});
	
	$('.edit-species').on('click', function(){
		$(".btn").focus();
		var rid = $(this).data('id');
		var card = $('#speciesform-card .card-body');
		card.before('<div class="ajax-spinner" style="display:none;"><img src="'+baseUrl+'/assets/images/fish-loader.gif" /></div>');
		var height=card.height();
		var width=card.width();
		card.css('opacity','0.1');		
		$('.ajax-spinner').show();
		$.ajax({
			type:"POST",
			url:baseUrl+"species/editSpecies",
			data: { 'rid' : rid },
		  }).done(function(data) {
			 $('#speciesform-card').empty();
			 $('#speciesform-card').html(data);
			 $('.ajax-spinner').hide();
			 $('#speciesform-card .card-body').css('opacity','1');
		  });
	});
	
	
	$('.sampling').on('click', function(event) {
		$('#samplinglist-form')[0].reset();
		var chk_url = site_url + "sampling/search_list";
		var cobj = $(this);
		var pname = cobj.attr("data-pname");
		var stype = cobj.attr("data-stype");
		$("#spondnamemodel").val(pname);
		$("#sspeciestypemodel").val(stype);
		$("#pondnameLabel").html("Sampling - ( "+pname+"-"+stype+")");
		var card = $('#samplinglist-form .modal-content');
		card.before('<div class="ajax-spinner" style="display:none;"><img src="'+baseUrl+'/assets/images/fish-loader.gif" /></div>');
		card.css('opacity','0.5');
		$('.ajax-spinner').show();
		
		$.ajax({
			type: "POST",
			data: { 'pondname' : pname },
			url: chk_url,
			}).done(function(result){
			$('.ajax-spinner').hide();
			card.css('opacity','1');
			var res = JSON.parse(result);
			if(res.status == "success"){
				var rst = res.result_set;
				if(rst.length==0){
					$("#countmodel").val("");
					$("#fishlengthmodel").val("");
					$("#avgweightmodel").val("");
					$("#samplingdatemodel").val("");
					$("#observationmodel").val("");
					$('#samplinglist-form .modal-content').show();
				}
				else{				
				var src = rst[0]._source || '';
				if(src.pondname == pname)
				{
					$('#samplinglist-form .modal-content').show();
					var count = src.count;
					var fish_length = src.fish_length;
					var avg_weight = src.avg_weight;
					var sampling_date=src.sampling_date;
					var sd = sampling_date;
					var t = sd + "000";
					t = parseInt(t);
					var dobj = new Date(t);
					var sdate =  (dobj.getMonth() + 1) + "/" + dobj.getDate() + "/" + dobj.getFullYear();
					var observation=src.observation;
					$("#countmodel").val(count);
					$("#fishlengthmodel").val(fish_length);
					$("#avgweightmodel").val(avg_weight);
					$("#samplingdatemodel").val(sdate);
					$("#observationmodel").val(observation);
				}
				}
			}
				
				
		}).fail(function(err){
			console.log(err);
		});
			
	});
	
	//distribution count validation
	$('#distribution-form #from_pond').on('change', function(){
		var pond_id = $(this).val();
		console.log(pond_id);
		if(pond_id){
		  $.ajax({
			  type: "POST",
			  url: baseUrl+"distribution/getcurrentStock",
			  data: {
				  'pond_id': pond_id
			  },
		  }).done(function(data) {
			  var returnedData = JSON.parse(data);
   			  console.log(returnedData);			  
			  var count = returnedData.result_set[0]._source.count;
			  var species_id = returnedData.result_set[0]._source.species_id;
				$('#species_type').val(species_id);
				$('#species_type').prop('disabled', 'disabled');
				$('#species_type').after('<input type="hidden" id="species_type_hidden" name="species_type" value="'+species_id+'"/>');
			  $('#available_count').val(parseInt(count));
			  $('.available_count').text("Available count is ( " + count+")");
  			  if(count!="undefined")
			  checkFishCount_distribution();
		  });		
		}else{
			$('#species_type').val("");
			$('#species_type').removeAttr('disabled');
			$('.count_exceed_err1').hide();
			$('.available_count').text('');
		}
	});
	
	//harvest count validation
	$('#harvest-form #pondname').on('change', function(){
		var pond_id = $(this).val();
		console.log(pond_id);
		if(pond_id){
		  $.ajax({
			  type: "POST",
			  url: baseUrl+"harvest/getcurrentStock",
			  data: {
				  'pond_id': pond_id
			  },
		  }).done(function(data) {
			  var returnedData = JSON.parse(data);
   			  console.log(returnedData);			  
			  var count = returnedData.result_set[0]._source.count;
			  var species_id = returnedData.result_set[0]._source.species_id;
				$('#species_type').val(species_id);
				$('#species_type').prop('disabled', 'disabled');
				$('#species_type').after('<input type="hidden" id="species_type_hidden" name="species_type" value="'+species_id+'"/>');
			  $('#available_count').val(parseInt(count));
			  $('.available_count').text("Available count is ( " + count+")");
  			  if(count!="undefined")
			  checkFishCount_harvest();
		  });		
		}else{
			$('.count_exceed_err2').hide();
			$('.available_count').text('');
			$('#species_type').val("");
			$('#species_type').removeAttr('disabled');
		}
	});
	
	$("input[name=stocked_weight]" ).on('change', function(){
		var weight = $(this).val();
		var posWeight;
		var posFlag=false;
		if(weight<0){
			posFlag= true;
			posWeight = Math.abs(weight);
		}
		var feedId =  $("#feedstock-form #feed_name option:selected").text();
		if(feedId){
			var t = $('.'+feedId);
			var getWeight = $(t).children('td').eq(2).text();
			if(posFlag && parseFloat(posWeight) > parseFloat(getWeight)){
				setTimeout(function(){
					$("input[name=stocked_weight]" ).after('<label id="stocked_weight_count_error" class="error" for="stocked_weight" style="display: block;">Please enter greater than current stock ('+getWeight+')</label>');
					$('#feedstock-form .btn-primary').attr('disabled',true);
				},50);
			}else{
				$('#stocked_weight_count_error').remove();
				$('#feedstock-form .btn-primary').attr('disabled',false);
			}
		}
	});
	

	//view feed

	$('.feedlist-table .view-modal').on('click', function(event) {
    var $row = $(this).closest("tr");    // Find the row
    var $tds = $row.find("td");
    $.each($tds, function(index,value) {
		switch(index) {
			case 0:
				$("#feednamemodel").html($(this).text());
			break;
			case 1:
				$("#feedtypemodel").html($(this).text());
			break;
			case 2:
				$("#feedsizemodel").html($(this).text());
			break;
			
		} 
    });
    

	});
	//view currentstock
	$('#cstock-table .view-modal').on('click', function(event) {
    var $row = $(this).closest("tr");    // Find the row
    var $tds = $row.find("td");
    $.each($tds, function(index,value) {
     		
		switch(index) {
			case 0:
				$("#pondnamemodel").html($(this).text());
			break;
			case 1:
				$("#countcsmodel").html($(this).text());
			break;
			case 2:
				$("#speciestypemodel").html($(this).text());
			break;
			case 3:
				$("#modifiedmodel").html($(this).text());
			break;
		} 
    });
	});
	
	//view species

	$('.species-table .view-modal').on('click', function(event) {
		var $row = $(this).closest("tr");    // Find the row
		var $tds = $row.find("td");
		$.each($tds, function(index,value) {
			switch(index) {
				case 0:
					$("#speciesnamemodel").html($(this).text());
				break;
				case 1:
					$("#speciesfeedmodel").html($(this).text());
				break;
				case 2:
					$("#speciesmaxweightmodel").html($(this).text());
				break;
				
			} 
		});
	});
	
	//main_j.js end
	//view pondlist
	$('#pondlist-table .view-modal').on('click', function(event) {
    var $row = $(this).closest("tr");    // Find the row
    var $tds = $row.find("td");
    $.each($tds, function(index,value) {
//        console.log($(this).text());				
		switch(index) {
			case 0:
				$("#pondnamemodel").html($(this).text());
			break;
			case 1:
				$("#pondlengthmodel").html($(this).text());
			break;
			case 2:
				$("#pondwidthmodel").html($(this).text());
			break;
			case 3:
				$("#ponddepthmodel").html($(this).text());
			break;
			case 4:
				$("#pondbuiltdatemodel").html($(this).text());
			break;
		} 
    });
	});
	
//view pondcleaning
	$('#pondcleaning-table .view-modal').on('click', function(event) {
    var $row = $(this).closest("tr");    // Find the row
    var $tds = $row.find("td");
    $.each($tds, function(index,value) {
//        console.log($(this).text());				
		switch(index) {
			case 0:
				$("#pondnamemodel").html($(this).text());
			break;
			case 1:
				$("#pondcleanedbymodel").html($(this).text());
			break;
			case 2:
				$("#pondcleaneddatemodel").html($(this).text());
			break;
		} 
    });
	});
	
		//view distribution
	$('.distribution-table .view-modal').on('click', function(event) {
    var $row = $(this).closest("tr");    // Find the row
    var $tds = $row.find("td");
    $.each($tds, function(index,value) {
        console.log($(this).text());				
		switch(index) {
			case 0:
				$("#frompondmodel").html($(this).text());
			break;
			case 1:
				$("#topondmodel").html($(this).text());
			break;
			case 2:
				$("#speciestypemodel").html($(this).text());
			break;
			case 3:
				$("#countmodel").html($(this).text());
			break;
			case 4:
				$("#updatedbymodel").html($(this).text());
			break;
			case 5:
				$("#distributeddatemodel").html($(this).text());
			break;
		} 
    });
	});
	
		//view fishstock
	$('#fishstock-table .view-modal').on('click', function(event) {
    var $row = $(this).closest("tr");    // Find the row
    var $tds = $row.find("td");
    $.each($tds, function(index,value) {
        console.log($(this).text());				
		switch(index) {
			case 0:
				$("#pondnamemodel").html($(this).text());
			break;
			case 1:
				$("#countmodel").html($(this).text());
			break;
			case 2:
				$("#speciestypemodel").html($(this).text());
			break;
			case 3:
				$("#weightmodel").html($(this).text());
			break;
			case 4:
				$("#fertilizermodel").html($(this).text());
			break;
			case 5:
				$("#watertypemodel").html($(this).text());
			break;
			case 6:
				$("#stockeddatemodel").html($(this).text());
			break;

		} 
    });
	});
	
		//view harvest
	$('.harvest-table .view-modal').on('click', function(event) {
    var $row = $(this).closest("tr");    // Find the row
    var $tds = $row.find("td");
    $.each($tds, function(index,value) {
//        console.log($(this).text());				
		switch(index) {
			case 0:
				$("#pondnamemodel").html($(this).text());
			break;
			case 1:
				$("#speciestypemodel").html($(this).text());
			break;
			case 2:
				$("#weightmodel").html($(this).text());
			break;
			case 3:
				$("#countmodel").html($(this).text());
			break;
			case 4:
				$("#harvesteddatemodel").html($(this).text());
			break;

		} 
    });
	});
	
			//view mortality
	$('.mortality-table .view-modal').on('click', function(event) {
    var $row = $(this).closest("tr");    // Find the row
    var $tds = $row.find("td");
    $.each($tds, function(index,value) {
//        console.log($(this).text());				
		switch(index) {
			case 0:
				$("#pondnamemodel").html($(this).text());
			break;
			case 1:
				$("#speciestypemodel").html($(this).text());
			break;
			case 2:
				$("#weightmodel").html($(this).text());
			break;
			case 3:
				$("#countmodel").html($(this).text());
			break;
			case 4:
				$("#reasonmodel").html($(this).text());
			break;
			case 5:
				$("#moralitydatemodel").html($(this).text());
			break;

		} 
    });
	});
	
	//feedstock-view
	
	$('.feedstock-table .view-modal').on('click', function(event) {
    var $row = $(this).closest("tr");    // Find the row
    var $tds = $row.find("td");
    $.each($tds, function(index,value) {
//        console.log($(this).text());				
		switch(index) {
			case 0:
				$("#feednamemodel").html($(this).text());
			break;
			case 1:
				$("#feedweightmodel").html($(this).text());
			break;
			case 2:
				$("#feedcurrentweightmodel").html($(this).text());
			break;

		} 
    });
	});
	
	//feeddistribution view
	
	$('.feeddist-table .view-modal').on('click', function(event) {
    var $row = $(this).closest("tr");    // Find the row
    var $tds = $row.find("td");
    $.each($tds, function(index,value) {
//        console.log($(this).text());				
		switch(index) {
			case 0:
				$("#feedpondmodel").html($(this).text());
			break;
			case 1:
				$("#feednamemodel").html($(this).text());
			break;
			case 2:
				$("#feedweightmodel").html($(this).text());
			break;
			case 3:
				$("#feeddatetmodel").html($(this).text());
			break;

		} 
    });
	});

	
	//main_p.js end
	
	//main_k.js start
	
	
	$("ul.pagination li a").addClass("page-link");
	
	 if( $("select[name=no_items]").length > 0 )
	 {
		 var per_page = $("select[name=no_items]").attr("data-per_page") || "";
		 if(per_page!=""){
			 $("select[name=no_items]").val(per_page);
		 }
	 }
	
	 $(document).on("change", "select[name=no_items]", function(){
		/* var v = $("select[name=per_page]").val();
		$("input[name=no_items]").val(v); */
		$("#srchfrm").submit();
	 });
	
	//mortality
	$('#count').on('keyup', function(){
		var fCount = $(this).val();
		checkFishCount_mortality();
	});
	
	// distribution
	$('#count').on('keyup', function(){
		var fCount = $(this).val();
		checkFishCount_distribution();
	});
	
	// harvest
	$('#count').on('keyup', function(){
		var fCount = $(this).val();
		checkFishCount_harvest();
	});

	
	$('#feeddist_weight').on('keyup', function(){
		var fWeight = $(this).val();
		checkFeedStock();
	});
	
  		
    $('.cancel').on('click', function() {
        var form = $(this).closest("form");
        form[0].reset();
    });
	

    $(document).on("mousemove", function(event) {
        var dw = $(document).width() / 15;
        var dh = $(document).height() / 15;
        var x = event.pageX / dw;
        var y = event.pageY / dh;
        $('.eye-ball').css({
            width: x,
            height: y
        });
    });

    //data table
    //$('#bootstrap-data-table').DataTable();

    $('.delete-modal').on('click', function() {
        var rid = $(this).data('id');
        $('#delete_rid').val(rid);
    });


    $('.modal').on('hide.bs.modal', function(e) {
        $('body').addClass('removepadding');
    });
	
	
		//remove text and element clear button (distribution and harvest)
	$(document).on('click','.cancel', function(){
        $("#species_type").removeAttr("disabled");
		$('.available_count').text('');
		$('.available_weight').text('');
		$( "#species_type_hidden" ).remove();
		$('.count_exceed_err').hide();
		$('.count_exceed_err1').hide();
		$('.count_exceed_err2').hide();
		$('.weight_exceed_err').hide();
		$('.form-control').removeClass('error');
		$('label.error').hide();
	});
	
	function checkFeedStock(){
		var fWeight = $('#feeddist_weight').val();
		var aWait = parseInt($('#available_weight').val());
		if(fWeight>aWait && aWait>0 && $('#feeddist-form #feed_stock_id').val()){
			$('#feeddist-form .btn-primary').attr('disabled',true);
			$('.weight_exceed_err').text("Weight is exceeding with available weight").show();
		}else{
			$('#feeddist-form .btn-primary').removeAttr('disabled');
			$('.weight_exceed_err').hide();
		}		
	}
	
	
	function checkFishCount_mortality(){
		var fCount = $('#count').val();
		var aCount = parseInt($('#available_count').val());
		if(fCount>aCount && $('#mortality-form #pondname').val()){
			$('#mortality-form .btn-primary').attr('disabled',true);
			$('.count_exceed_err').text("Count is exceeding with available count").show();
		}else{
			$('#mortality-form .btn-primary').removeAttr('disabled');
			$('.count_exceed_err').hide();
		}		
	}	
	
	function checkFishCount_distribution(){
		var fCount = $('#count').val();
		var aCount = parseInt($('#available_count').val());
		if(fCount>aCount && $('#distribution-form #from_pond').val()){
			$('#distribution-form .btn-primary').attr('disabled',true);
			$('.count_exceed_err1').text("Count is exceeding with available count").show();
		}else{
			$('#distribution-form .btn-primary').removeAttr('disabled');
			$('.count_exceed_err1').hide();
		}		
	}
	

	
	function checkFishCount_harvest(){
		var fCount = $('#count').val();
		var aCount = parseInt($('#available_count').val());
		if(fCount>aCount && $('#harvest-form #pondname').val()){
			$('#harvest-form .btn-primary').attr('disabled',true);
			$('.count_exceed_err2').text("Count is exceeding with available count").show();
		}else{
			$('#harvest-form .btn-primary').removeAttr('disabled');
			$('.count_exceed_err2').hide();
		}		
	}	


});