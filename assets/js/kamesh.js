jQuery(document).ready(function($) { 
	
	$("ul.pagination li a").addClass("page-link");
	
	$(document).on("submit", "#srchfrm", function(event){
		
	});
	
	$("#pondlist-form1").validate({
		rules: {
			pondname: {
				required: true,
			},
			width: {
				required: true,
				number: true
			},
			height: {
				required: true,
				number: true
			},
			depth: {
				required: true,
				number: true
			},
			built_date: {
				required: true,
			},
		},
		messages: {

			pondname: {
				required: "Please enter pond name",
			},
			width: {
				required: "Please enter pond width",
				number : "Please enter valid width"
			},
			height: {
				required: "Please enter pond width",
				number : "Please enter valid height"
			},
			depth: {
				required: "Please enter pond width",
				number : "Please enter valid depth"
			},
			built_date: {
				required: "Please select date",
			}
	},
	errorPlacement: function(error, element) {
		
		if (element.attr("name") == "built_date" )
			error.insertAfter(".datetimepicker-icon");
		else
			error.insertAfter(element);
		}

	});
	
});