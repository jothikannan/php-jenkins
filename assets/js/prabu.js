jQuery(document).ready(function($) { 


	$("#pondlist-form").validate({
		rules: {
			pondname: {
				required: true,
			},
			width: {
				required: true,
				number: true
			},
			height: {
				required: true,
				number: true
			},
			depth: {
				required: true,
				number: true
			},
			built_date: {
				required: true,
			},
		},
		messages: {

			pondname: {
				required: "Please enter pond name",
			},
			width: {
				required: "Please enter pond width",
				number : "Please enter valid width"
			},
			height: {
				required: "Please enter pond width",
				number : "Please enter valid height"
			},
			depth: {
				required: "Please enter pond width",
				number : "Please enter valid depth"
			},
			built_date: {
				required: "Please select date",
			}
	},
	errorPlacement: function(error, element) {
		
		if (element.attr("name") == "built_date" )
			error.insertAfter(".datetimepicker-icon");
		else
			error.insertAfter(element);
		}
	});
	
	$("#pondcleaning-form").validate({
		rules: {
			pondname: {
				required: true,
			},
			cleanedby: {
				required: true,
			},
			cleaned_date: {
				required: true,
			},
		},
		messages: {

			pondname: {
				required: "Please select pond name",
			},
			cleanedby: {
				required: "Please enter pond cleaned by",
			},
			cleaned_date: {
				required: "Please select date",
			},
	},
	errorPlacement: function(error, element) {
		
		if (element.attr("name") == "cleaned_date" )
			error.insertAfter(".datetimepicker-icon");
		else
			error.insertAfter(element);
		}

	});
	
	

});