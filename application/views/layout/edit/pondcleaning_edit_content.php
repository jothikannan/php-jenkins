<div class="card-header"> <strong class="card-title"><?php echo fb_text("edit_pond_cleaning"); ?></strong> </div>
<div class="card-body">
  <form name="pondcleaning" id="pondcleaning-form" method="post" action="<?php echo base_url('pondcleaning/update');?>">
    <div class="form-row">
      <div class="form-group col-md-6">
          <label for="pondname"><?php echo fb_text("pond_name"); ?><span class="error">*</span></label>
          <?php
						$table_name = "ponds";
						$pond_list = fb_combo_arr($table_name);						
					?>
          <select name="pondname" id="pondname" class="form-control">
            <option selected value=""><?php echo fb_text("choose"); ?></option>
            <?php foreach($pond_list as $k => $v){ ?>
            <option value='<?php echo $k; ?>' <?php if($record['pond_id']==$k){ ?> selected="selected" <?php } ?>><?php echo $v?></option>
            <?php } ?>
          </select>
      </div>
      <div class="form-group col-md-6">
		<label for="cleanedby"><?php echo fb_text("cleaned_by"); ?><span class="error">*</span></label>
        <input type="text" class="form-control" name="cleanedby" placeholder="<?php echo fb_text("cleaned_by"); ?>" value="<?= $record['cleanedby']; ?>">
      </div>
    </div>
    <div class="form-row">
      <div class="form-group col-md-6">
        <label for="inputCity"><?php echo fb_text("cleaned_date"); ?><span class="error">*</span></label>
        <div class="input-group date" id="datetimepicker" data-target-input="nearest">
          <input type="text" class="form-control datetimepicker-input" name="cleaned_date" value="<?php echo fb_convert_date($record["cleaned_date"]); ?>" data-target="#datetimepicker"/>
          <div class="input-group-append datetimepicker-icon add-on" data-target="#datetimepicker" data-toggle="datetimepicker">
            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
      </div>
    </div>
    <input type="hidden" name="rkey" value="<?=$rkey?>">
    <button type="submit" class="btn btn-primary"><?php echo fb_text("save"); ?></button>
    <button type="button" class="btn btn-secondary cancel"><?php echo fb_text("clear"); ?></button>
  </form>
</div>
<script type="application/javascript">

    $.validator.addMethod("chkduplicate", function(value, element, arg) {
		console.log(value);
		var pms = JSON.parse(arg);
		pms["search"] = value ;
		var rkey = $(element).closest("form").find("input[name=rkey]").val() || "";
		pms["rkey"] = rkey;
		var chk_url = site_url + "common/check_duplicate";
		var flg = false;
		$.ajax({
		    	type:"GET",
		    	data: pms,
		    	url: chk_url,
				async:false,
		}).done(function(resp){
			var rdata = JSON.parse(resp);
			console.log(resp);
			console.log(rdata.status);
			if(rdata.status == "success"){
				console.log("success");
				flg = true;
			} else{
				console.log("fail");
				flg = false;
			}
			
		}).fail(function(err){
			console.log(err);
		});
		console.log("123>>>>>>");
		//console.log(pms);
		return flg;
    }, "This field value already exists.");
	
	$("#pondcleaning-form").validate({
		rules: {
			pondname: {
				required: true,
			},
			cleanedby: {
				required: true,
			},
			cleaned_date: {
				required: true,
			},
		},
		messages: {

			pondname: {
				required: disp_text("err_pname"),
			},
			cleanedby: {
				required: disp_text("err_cleanedby"),
			},
			cleaned_date: {
				required: disp_text("err_date"),
			},
	},
	errorPlacement: function(error, element) {
		
		if (element.attr("name") == "cleaned_date" )
			error.insertAfter(".datetimepicker-icon");
		else
			error.insertAfter(element);
		}

	});	
	
	if($('#datetimepicker').length>0){
	  $('#datetimepicker').datetimepicker({
		  format: 'L',
		  keepOpen : false
	   });
	}
	
	$(document).on('click','.cancel', function(){
		$("#pondcleaning-form").find('input, select, textarea').val('');
	});
</script>	