    <?php 
	//$activities= getActivity();
	if(!empty($activities)) {
	foreach($activities as $activity): 
	$_source = $activity['_source'];
	//print_r($_source);
	$activityArr= array();
	$activityArr= unserialize($_source['activity_ref']);
	//print_r($activityArr);
	?>
		<div class="feed-item">
		  <div class="date">
			<?=  date("d F Y H:i:s", $_source['createdtime']) ?>
		  </div>
		  <div class="text">
			<?= $activityArr['msg']; ?>
		  </div>
		</div>
		<?php endforeach; 
		} else {
			echo '<div class="alert alert-info" role="alert"> No activity found yet </div>';
		}
	?>
