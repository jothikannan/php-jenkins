<div id="right-panel" class="right-panel">
  <?php 
  $this->benchmark->mark('top_menu_start');
  $this->load->view('./include/top_menu'); 
  $this->benchmark->mark('top_menu_end');
  ?>
  <div class="content mt-3">
    <div class="row">
      <div class="col-md-6 col-lg-4">
        <div id="harvest-carousel" class="carousel slide" data-ride="carousel" data-interval="false"> 
          
          <!-- Left and right controls --> 
          <!--          <div class="pull-right"> <a class="carousel-prev" href="#harvest-carousel" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a> <a class="carousel-next" href="#harvest-carousel" data-slide="next"> <span class="carousel-control-next-icon"></span> </a> </div>--> 
          <!-- The slideshow -->
          <div class="carousel-inner">
            <div class="carousel-item active">
              <div>
                <div class="card">
                  <div class="card-header"> <strong class="card-title"><?php echo fb_text("current_stock"); ?></strong> </div>
                  <div class="card-body">
                    <table class="table">
                      <thead class="thead-dark">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col"><?php echo fb_text("pond_name"); ?></th>
                          <th scope="col"><?php echo fb_text("species"); ?></th>
                         <th scope="col"><?php echo fb_text("count"); ?></th>
                          <th scope="col"><?php echo fb_text("weight"); ?></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
						if(!empty($stocks)):
							$i=0;
							foreach($stocks as $stock): 
								$source = $stock["_source"];
								$i++;
					  ?>
								<tr>
								  <th scope="row"><?= $i; ?></th>
								  <td><?= $source['pondname']?></td>
								  <td><?= $source['species_type']?></td>
								  <td><?= $source['count']?></td>
								  <?php if(isset($asampling[$source['pondname']])): 
								  $puwt = $asampling[$source['pondname']]["per_unit"];
								  $tcnt = $source['count'];
								  ?>
								  <td><?php echo disp_weight($tcnt, $puwt); ?></td>
								  <?php else: ?>
								  <td> - </td>
								  <?php endif; ?>
								</tr>
								<?php //get_sampling($source['pondname']); ?>
							<?php endforeach; ?>
						<?php else: ?>	
						 <tr>
							<th colspan="4"><div class="alert alert-info" role="alert">No records found.</div></th>
						 </tr>
						<?php endif; ?>	
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-4">
	    <?php if(!empty($wk_reports)): ?>
        <div id="mortality-carousel" class="carousel slide" data-ride="carousel" data-interval="false"> 
          
          <!-- Left and right controls -->
          <div class="pull-right mortality-arrow"> <a class="carousel-prev" href="#mortality-carousel" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a> <a class="carousel-next" href="#mortality-carousel" data-slide="next"> <span class="carousel-control-next-icon"></span> </a> </div>
          <!-- The slideshow -->
          <div class="carousel-inner">
		   <?php 
		   $crsl_cnt = 1;
		   foreach($wk_reports as $mname => $mresult): ?>
		      <?php 
			  foreach($mresult as $wk_name => $wk_result): 
			     $crsl_class = ($crsl_cnt==1)? "carousel-item active" : "carousel-item";
				 $crsl_cnt++;
			  ?>
		   <div class="<?php echo $crsl_class; ?>">
              <div>
                <div class="card">
                  <div class="card-header"> <strong class="card-title"><?php echo fb_text($mname)." - ".fb_text($wk_name) ?></strong> </div>
                  <div class="card-body">
                    <table class="table">
                      <thead class="thead-dark">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col"><?php echo fb_text("pond_name"); ?></th>
                          <th scope="col"><?php echo fb_text("count"); ?></th>
                          <th scope="col"><?php echo fb_text("weight"); ?></th>
                        </tr>
                      </thead>
                      <tbody>
					    <?php 
						$rcnt = 1;
						foreach($wk_result as $wk_row ): 
						   $wk_src = $wk_row["_source"];
						?>
                        <tr>
                          <th scope="row"><?php echo $rcnt; ?></th>
                          <td><?php echo $wk_src["pondname"]; ?></td>
                          <td><?php echo $wk_src["count"]; ?></td>
                          <td><?php echo $wk_src["weight"]; ?></td>
                        </tr>
						<?php 
						$rcnt++;
						endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
		     <?php endforeach; ?>
		   <?php endforeach; ?>
		  </div>
        </div>
        
		<?php else: ?>
		
		<div id="mortality-carousel" class="carousel slide" data-ride="carousel" data-interval="false"> 
          
          <!-- Left and right controls -->
          <div class="pull-right mortality-arrow"> <a class="carousel-prev" href="#mortality-carousel" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a> <a class="carousel-next" href="#mortality-carousel" data-slide="next"> <span class="carousel-control-next-icon"></span> </a> </div>
          <!-- The slideshow -->
          <div class="carousel-inner">
            <div class="carousel-item active">
              <div>
                <div class="card">
                  <div class="card-header"> <strong class="card-title"><?php echo fb_text("harvest")." - ".fb_text("this_week") ?></strong> </div>
                  <div class="card-body">
                    <table class="table">
                      <thead class="thead-dark">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col"><?php echo fb_text("pond_name"); ?></th>
                          <th scope="col"><?php echo fb_text("count"); ?></th>
                          <th scope="col"><?php echo fb_text("weight"); ?></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row" colspan="4"><div class="alert alert-info" role="alert">No records found.</div></th>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div>
                <div class="card">
                  <div class="card-header"> <strong class="card-title"><?php echo fb_text("mortality")." - ".fb_text("this_week") ?></strong> </div>
                  <div class="card-body">
                    <table class="table">
                      <thead class="thead-dark">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col"><?php echo fb_text("pond_name"); ?></th>
                          <th scope="col"><?php echo fb_text("count"); ?></th>
                          <th scope="col"><?php echo fb_text("weight"); ?></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row" colspan="4"><div class="alert alert-info" role="alert">No records found.</div></th>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      
		
		<?php endif; ?>
	  
	  </div>
      <div class="col-md-6 col-lg-4"> &nbsp;
        <div class="card">
          <div class="card-body">
            <h4 class="mb-3"><?php echo fb_text("current_stock"); ?> (<?php echo fb_text("count"); ?>) </h4>
            <canvas id="doughutChart"></canvas>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-12">
      <div class="card" id="activity-feed-card">
      <div class="card-body">
          <h2>Activity </h2>
          <div class="activity-feed" id="activity-feed">
         <?php $this->load->view("layout/activity_content"); ?>

         </div>
         </div>
      </div>
	  <?php if(!empty($activities)): ?>
        <div class="col-md-6 col-lg-12">
      	 <button type="button" id="loadmore-activity" class="btn btn-outline-primary offset-md-6" data-val="2">Load More</button>
        </div>
        <div class="col-md-6 col-lg-12">
	        &nbsp;
        </div>
	  <?php endif; ?>
    </div>


     
    </div>
  </div>
  
</div>
<!-- /#right-panel --> 

<!-- Right Panel -->