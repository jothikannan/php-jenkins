<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['dashboard'] = "Tablero";
$lang['menus'] = "Menús";

// Dashboard
$lang['ponds']="Estanques";
$lang['pond_list'] = "Lista de estanques";
$lang['pond_cleaning'] = "Limpieza de estanques";
$lang['species'] = "Especies";
$lang['feed'] = "Alimentar";
$lang['feed_list'] = "Lista de feeds";
$lang['feed_stock_list'] = "Lista de existencias de alimentos";
$lang['feed_distribution'] = "Distribución de alimento";
$lang['stock'] = "Valores";
$lang['current_stock'] = "Stock actual";
$lang['fish_stock'] = "Recursos pesqueros";
$lang['distribution'] = "Distribución";
$lang['harvest'] = "Cosecha";
$lang['mortality'] = "Mortalidad";
$lang['action']="Acción";

//pond list
$lang['pond_name']="Nombre del estanque";
$lang['length']="Longitud (en pies)";
$lang['width']="Anchura (en pies)";
$lang['depth']="Profundidad (en pies)";
$lang['built_date']="Fecha de construcción";
$lang['add_pond']="Añadir estanque";
$lang['width_in_feet']="Ancho en pies";
$lang['depth_in_feet']="Profundidad en pies";
$lang['length_in_feet']="Longitud en pies";
$lang['edit_pond']="Editar estanque";

//pond cleaning
$lang['cleaned_by']="Limpiado por";
$lang['cleaned_date']="Fecha de limpieza";
$lang['date_of_clenaing']="Fecha de limpieza";
$lang['add_pond_cleaning']="Añadir limpieza de estanque";
$lang['edit_pond_cleaning']="Editar limpieza de estanque";

//species
$lang['species_name'] = "Nombre de la especie";
$lang['feed_name'] = "nombre de la alimentación";
$lang['market_size'] = "tamaño del mercado (en gramos)";
$lang['add_species'] = "Añadir especies";
$lang['edit_species'] = "Editar especies";

//feed list
$lang['feed_type'] = "Tipo de fuente";
$lang['size'] = "tamaño (en Milímetro)";
$lang['add_feed'] = "Agregar feed";
$lang['edit_feed']="Editar alimentación";

//feed stock list
$lang['stocked_weight'] = "Peso almacenado";
$lang['current_weight'] = "Peso actual";
$lang['date'] = "Fecha";
$lang['weight_in_kg'] = "Peso en KG";
$lang['add_feed_stock'] = "Agregar stock de feed";
$lang['edit_feed_stock'] = "Editar Stock de Feed";

//feed distribution
$lang['feed_stock_distribution_list'] = "Lista de distribución de existencias de alimentos";
$lang['add_feed_stock_distribution'] = "Agregar distribución de existencias de piensos";
$lang['edit_feed_stock_distribution'] = "Editar distribución de stock de alimentación";
$lang['weight'] = "Peso (en gramos)";

//current stock
$lang['count'] = "contar";
$lang['species_type'] = "Tipo de Especie";
$lang['modified'] = "Modificado";

//fish stock
$lang['fertilizer'] = "Fertilizante";
$lang['water_type'] = "Tipo de agua";
$lang['weight_in_grams'] = "Peso en gramos";
$lang['add_fish_stock'] = "Agregar Stock de Pescado";
$lang['edit_fish_stock'] = "Editar stock de peces";

//distribution
$lang['from_pond'] = "De la charca";
$lang['to_pond'] = "Estanque";
$lang['updated_by'] = "Actualizado por";
$lang['add_distribution'] = "Agregar distribución";

//harvest
$lang['add_harvest'] = "Añadir cosecha";

//morality
$lang['add_mortality'] = "Agregar mortalidad";
$lang['reason'] = "Razón";

//sampling
$lang['avg_weight'] = "Peso promedio (en gramos)";
$lang['fish_length'] = "Longitud de pez (en centímetros)";
$lang['fish_count'] = "Conteo de peces";
$lang['observation'] = "Observación";

// Common
$lang["show"] = "espectáculo";
$lang["entries"] = "entradas";
$lang["search"] = "Buscar";
$lang["success"] = "Grabación creada";
$lang["failed"] = "Error de inserción, ¡inténtelo de nuevo!";
$lang["update_success"] = "Registro actualizado";
$lang["update_failed"] = "La actualización falló, ¡Inténtalo de nuevo!";
$lang["delete_success"] = "Registro eliminado";
$lang["delete_failed"] = "Falló, por favor intente nuevamente!";
$lang["choose"] = "escoger...";

//delete popup
$lang["delete"] = "borrar";
$lang["dmsg"] = "¿Estás seguro que quieres eliminar esto?";
$lang["confirm"] = "confirmar";
$lang["cancel"] = "cancelar";

//buttons
$lang["save"] = "salvar";
$lang["clear"] = "cancelar";
$lang["close"] = "cerca";

$lang['this_week'] = "esta semana";
$lang['last_week'] = "la semana pasada";

// Settings
$lang['settings'] = "ajustes";
$lang['settings_list'] = "lista de configuraciones";
$lang['rest_api_cache'] = "Descanso API Cache";
$lang['output_profiler'] = "Perfilador de salida";