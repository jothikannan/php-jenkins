<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['dashboard'] = "டாஷ்போர்டு";
$lang['menus'] = "மெனுக்கள்";

// Dashboard
$lang['ponds'] = "குளம்";
$lang['pond_list'] = "குளம் பட்டியல்";
$lang['pond_cleaning'] = "குளம் சுத்தம் செய்தல்";
$lang['species'] = "மீன் இனங்கள்";
$lang['feed'] = "மீன் உணவு";
$lang['feed_list'] = "மீன் உணவு பட்டியல்";
$lang['feed_stock_list'] = "மீன் உணவு கையிருப்பு பட்டியல்";
$lang['feed_distribution'] = "மீன் உணவு விநியோகிக்கப்பட்ட பட்டியல்";
$lang['stock'] = "கையிருப்பு";
$lang['current_stock'] = "தற்போதைய கையிருப்பு";
$lang['fish_stock'] = "மீன் கையிருப்பு";
$lang['distribution'] = "விநியோகம்";
$lang['harvest'] = "மீன் விற்பனை";
$lang['mortality'] = "மீன் இறப்பு";
$lang['action'] = "செயல்கள்";

//pond list
$lang['pond_name'] = "குளத்தின் பெயர்";
$lang['length'] = "நீளம் (அடி)";
$lang['width'] = "அகலம் (அடி)";
$lang['depth'] = "ஆழம் (அடி)";
$lang['built_date'] = "கட்டப்பட்ட தேதி";
$lang['add_pond'] = "குளம் சேர்க்க";
$lang['width_in_feet'] = "அடிவாரத்திண் அகலம்";
$lang['depth_in_feet'] = "அடி ஆழத்திண் அகலம்";
$lang['length_in_feet'] = "அடி நீளம் ";
$lang['edit_pond'] = "குளம் திருத்தம் செய்ய";

//pond cleaning
$lang['cleaned_by'] = "சுத்தம் செய்தவர் பெயர் ";
$lang['cleaned_date'] = "சுத்தம் செய்த தேதி";
$lang['date_of_clenaing'] = "சுத்தம் செய்த தேதி";
$lang['add_pond_cleaning'] = "குளம் சுத்தம் சேர்க்க";
$lang['edit_pond_cleaning'] = "குளம் சுத்தம் திருத்தம் செய்ய";

//feed list
$lang['feed_name'] = "மீன் உணவு பெயர்";
$lang['feed_type'] = "மீன் உணவு வகை";
$lang['size'] = "அளவு (மில்லிமீட்டரில்)";
$lang['add_feed'] = "மீன் உணவு சேர்க்க";
$lang['edit_feed']="மீன் உணவு திருத்தம் செய்ய ";

//feed stock list
$lang['stocked_weight'] = "கையிருப்பு எடை";
$lang['current_weight'] = "தற்போதைய எடை";
$lang['weight_in_kg'] = "எடை கிலோகிராம் அளவு";
$lang['add_feed_stock'] = "மீன் உணவு சேர்க்க";
$lang['edit_feed_stock'] = "மீன் உணவு திருத்தம் செய்ய";


//feed distribution
$lang['feed_stock_distribution_list'] = "மீன் உணவு விநியோகிக்கப்பட்ட பட்டியல்";
$lang['add_feed_stock_distribution'] = "மீன் உணவு விநியோகம் சேர்க்க";
$lang['edit_feed_stock_distribution'] = "மீன் உணவு விநியோகம் திருத்தம் செய்ய";
$lang['weight'] = "எடை (கிராம்களில்) ";

//species
$lang['species_name'] = "மீன் இனங்கள் பெயர்";
$lang['market_size'] = "சந்தை அளவு (கிராம்களில்)";
$lang['add_species'] = "மீன் இனங்கள் சேர்க்க";
$lang['edit_species'] = "மீன் இனங்கள் திருத்தம் செய்ய";

//current stock
$lang['count'] = "எண்ணிக்கை";
$lang['species_type'] = "மீன் இனங்கள் வகை";
$lang['modified'] = "திருத்தப்பட்ட தேதி";

//fish stock
$lang['fertilizer'] = "உரம்";
$lang['water_type'] = "நீர் வகை";
$lang['weight_in_grams'] = "எடை கிரா மில் ";
$lang['add_fish_stock'] = "மீன் இருப்பு  சேர்க்க";
$lang['edit_fish_stock'] = "மீன் இருப்பு திருத்த";

//distribution
$lang['from_pond'] = "மீன் எடுக்கப்பட்ட குளம்";
$lang['to_pond'] = "மீன் மாற்றப்பட்ட குளம்";
$lang['updated_by'] = "யாரால் புதுப்பிக்கப்பட்டது";
$lang['date'] = "தேதி";
$lang['add_distribution'] = "விநியோகம் சேர்க்க";

//harvest
$lang['add_harvest'] = "மீன் விற்பனை சேர்க்க";

//morality
$lang['add_mortality'] = "மீன் இறப்பு சேர்க்க";
$lang['reason'] = "மீன் இறப்பு காரணம்";

//sampling
$lang['avg_weight'] = "சராசரி எடை (கிராம்களில் ) ";
$lang['fish_length'] = "மீன் நீளம் (சென்டி மீட்டர் ) ";
$lang['fish_count'] = "மீன் எண்ணிக்கை";
$lang['observation'] = "கருத்துகள்";

// Common
$lang["show"] = "காண்பி ";
$lang["entries"] = "பதிவுகள்";
$lang["search"] = "தேடு";
$lang["success"] = "பதிவு உருவாக்கப்பட்டது";
$lang["failed"] = "பதிவு செருக முடியவில்லை, மீண்டும் முயற்சிக்கவும்!";
$lang["update_success"] = "பதிவு புதுப்பிக்கப்பட்டது";
$lang["update_failed"] = "பதிவு புதுப்பிப்பு தோல்வியடைந்தது, தயவு செய்து மீண்டும் முயற்சிக்கவும்!";
$lang["delete_success"] = "பதிவு நீக்கப்பட்டது";
$lang["delete_failed"] = "பதிவு நீக்கல் தோல்வியடைந்தது, தயவுசெய்து மீண்டும் முயற்சிக்கவும்!";
$lang["choose"] = "தேர்ந்தெடு...";

//delete popup
$lang["delete"] = "பதிவு நீக்க";
$lang["dmsg"] = "இதை நிச்சயமாக நீக்கவா?";
$lang["confirm"] = "உறுதி";
$lang["cancel"] = "தவிர்";

//buttons
$lang["save"] = "சேமி";
$lang["clear"] = "மாற்றியமை";
$lang["close"] = "மறை";

$lang['this_week'] = "இந்த வாரம்";
$lang['last_week'] = "கடந்த வாரம்";

// Settings
$lang['settings'] = "அமைப்புகள்";
$lang['settings_list'] = "அமைப்புகளின் பட்டியல்";
$lang['rest_api_cache'] = "ரெஸ்ட் API கேச்";
$lang['output_profiler'] = "அவுட்புட்  ப்ரொபைலிற்";