<?php

class MY_Controller extends CI_Controller
{
	public function __construct() {
		parent::__construct();
		$this->config->load("fb_settings", TRUE);
		$asettings = $this->config->item("settings", "fb_settings");
		if($asettings['output_profiler']){
			$sections = array(
					'config'  => false,
					'queries' => false,
					'benchmarks' => TRUE,
					'controller_info' => FALSE,
					'get' => FALSE,
					'http_headers' => FALSE,
					'memory_usage' => FALSE,
					'post' => FALSE,
					'uri_string' => FALSE,
					'session_data' => false,
					'query_toggle_count' => false
			);

			if (!$this->input->is_ajax_request()) {
				$this->output->set_profiler_sections($sections);
				$this->output->enable_profiler(TRUE);
			}
		}
		$alang = fb_cur_lang();
		$this->lang->load('main', $alang["lang_name"]);
		$this->load->library('parser');
		$this->parser->set_delimiters("__","__");
	}
}