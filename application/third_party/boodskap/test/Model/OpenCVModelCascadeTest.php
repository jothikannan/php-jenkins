<?php
/**
 * OpenCVModelCascadeTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * User API
 *
 * Boodskap IoT Platform (User API)
 *
 * OpenAPI spec version: 1.0.6
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.3.1
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * OpenCVModelCascadeTest Class Doc Comment
 *
 * @category    Class */
// * @description OpenCVModelCascade
/**
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class OpenCVModelCascadeTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "OpenCVModelCascade"
     */
    public function testOpenCVModelCascade()
    {
    }

    /**
     * Test attribute "label"
     */
    public function testPropertyLabel()
    {
    }

    /**
     * Test attribute "description"
     */
    public function testPropertyDescription()
    {
    }

    /**
     * Test attribute "color"
     */
    public function testPropertyColor()
    {
    }

    /**
     * Test attribute "min_size"
     */
    public function testPropertyMinSize()
    {
    }

    /**
     * Test attribute "max_size"
     */
    public function testPropertyMaxSize()
    {
    }

    /**
     * Test attribute "scale_factor"
     */
    public function testPropertyScaleFactor()
    {
    }

    /**
     * Test attribute "min_neighbours"
     */
    public function testPropertyMinNeighbours()
    {
    }

    /**
     * Test attribute "flags"
     */
    public function testPropertyFlags()
    {
    }

    /**
     * Test attribute "font_face"
     */
    public function testPropertyFontFace()
    {
    }

    /**
     * Test attribute "font_scale"
     */
    public function testPropertyFontScale()
    {
    }

    /**
     * Test attribute "min_score"
     */
    public function testPropertyMinScore()
    {
    }

    /**
     * Test attribute "overlay_text"
     */
    public function testPropertyOverlayText()
    {
    }

    /**
     * Test attribute "overlay_type"
     */
    public function testPropertyOverlayType()
    {
    }

    /**
     * Test attribute "cascade_size"
     */
    public function testPropertyCascadeSize()
    {
    }
}
