# Counter

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**users** | **int** |  | 
**devices** | **int** |  | 
**udp_messages** | **int** |  | 
**mqtt_messages** | **int** |  | 
**http_messages** | **int** |  | 
**fcm_messages** | **int** |  | 
**coap_messages** | **int** |  | 
**tcp_messages** | **int** |  | 
**commands** | **int** |  | 
**domains** | **int** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


