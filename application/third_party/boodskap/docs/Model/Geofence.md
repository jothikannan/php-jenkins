# Geofence

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | 
**geo_type** | **string** |  | 
**label** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**coordinates** | **string** |  | [optional] 
**radius** | **string** |  | [optional] 
**geometries** | **string** |  | [optional] 
**created_at** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


