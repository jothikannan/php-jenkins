# TemplateQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**method** | **string** |  | 
**system_template** | **bool** |  | [optional] 
**template_name** | **string** |  | 
**merge_content** | **string** | Merging JSON string | [optional] 
**extra_path** | **string** |  | [optional] 
**params** | [**\Swagger\Client\Model\Param[]**](Param.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


