# FileContent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content** | **string** |  | 
**format** | **string** |  | 
**ispublic** | **bool** |  | [optional] 
**id** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


