# User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **string** |  | 
**password** | **string** |  | [optional] 
**first_name** | **string** |  | [optional] 
**last_name** | **string** |  | [optional] 
**country** | **string** |  | [optional] 
**state** | **string** |  | [optional] 
**city** | **string** |  | [optional] 
**address** | **string** |  | [optional] 
**zipcode** | **string** |  | [optional] 
**primary_phone** | **string** |  | [optional] 
**locale** | **string** |  | [optional] 
**timezone** | **string** |  | [optional] 
**work_start** | **int** |  | [optional] 
**work_end** | **int** |  | [optional] 
**work_days** | **int[]** |  | [optional] 
**roles** | **string[]** |  | [optional] 
**groups** | **int[]** |  | [optional] 
**registered_stamp** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


