# Swagger\Client\AddMembersSToUserGroupApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addMembersToUserGroup**](AddMembersSToUserGroupApi.md#addMembersToUserGroup) | **POST** /user/group/add/{atoken}/{ouid}/{gid} | Add Members(s) to User Group


# **addMembersToUserGroup**
> \Swagger\Client\Model\Success addMembersToUserGroup($atoken, $ouid, $gid, $user_ids)

Add Members(s) to User Group

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AddMembersSToUserGroupApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$ouid = "ouid_example"; // string | Owner User ID
$gid = 56; // int | Group Id
$user_ids = array(new \Swagger\Client\Model\string[]()); // string[] | Array of user IDs

try {
    $result = $apiInstance->addMembersToUserGroup($atoken, $ouid, $gid, $user_ids);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AddMembersSToUserGroupApi->addMembersToUserGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **ouid** | **string**| Owner User ID |
 **gid** | **int**| Group Id |
 **user_ids** | **string[]**| Array of user IDs |

### Return type

[**\Swagger\Client\Model\Success**](../Model/Success.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

