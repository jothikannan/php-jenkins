# Swagger\Client\SearchByTemplateApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**searchByTemplate**](SearchByTemplateApi.md#searchByTemplate) | **POST** /search/template/{atoken}/{type}/ | Search by Template


# **searchByTemplate**
> \Swagger\Client\Model\SearchResult searchByTemplate($atoken, $type, $query, $id, $repositary, $mapping)

Search by Template

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SearchByTemplateApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$type = "type_example"; // string | 
$query = new \Swagger\Client\Model\TemplateQuery(); // \Swagger\Client\Model\TemplateQuery | TemplateQuery JSON
$id = 56; // int | Required if **type** is one of **[ message | command | record ]**
$repositary = "repositary_example"; // string | Required if **type** is one of **GLOBAL**
$mapping = "mapping_example"; // string | Required if **type** is one of **GLOBAL**

try {
    $result = $apiInstance->searchByTemplate($atoken, $type, $query, $id, $repositary, $mapping);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SearchByTemplateApi->searchByTemplate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **type** | **string**|  |
 **query** | [**\Swagger\Client\Model\TemplateQuery**](../Model/TemplateQuery.md)| TemplateQuery JSON |
 **id** | **int**| Required if **type** is one of **[ message | command | record ]** | [optional]
 **repositary** | **string**| Required if **type** is one of **GLOBAL** | [optional]
 **mapping** | **string**| Required if **type** is one of **GLOBAL** | [optional]

### Return type

[**\Swagger\Client\Model\SearchResult**](../Model/SearchResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

