# Swagger\Client\RegisterNewDomainApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**register**](RegisterNewDomainApi.md#register) | **POST** /domain/register | Register New Domain


# **register**
> \Swagger\Client\Model\UserDomain register($user)

Register New Domain

Returns all domains from the system that the user has access to

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RegisterNewDomainApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$user = new \Swagger\Client\Model\User(); // \Swagger\Client\Model\User | User object

try {
    $result = $apiInstance->register($user);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RegisterNewDomainApi->register: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | [**\Swagger\Client\Model\User**](../Model/User.md)| User object |

### Return type

[**\Swagger\Client\Model\UserDomain**](../Model/UserDomain.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

