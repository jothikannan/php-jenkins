# Swagger\Client\AddMembersSToDomainDeviceGroupApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addMembersToDomainDeviceGroup**](AddMembersSToDomainDeviceGroupApi.md#addMembersToDomainDeviceGroup) | **POST** /domain/device/group/add/{atoken}/{gid} | Add Members(s) to Doain Device Group


# **addMembersToDomainDeviceGroup**
> \Swagger\Client\Model\Success addMembersToDomainDeviceGroup($atoken, $gid, $device_ids)

Add Members(s) to Doain Device Group

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\AddMembersSToDomainDeviceGroupApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$gid = 56; // int | Group Id
$device_ids = array(new \Swagger\Client\Model\string[]()); // string[] | Array of device IDs

try {
    $result = $apiInstance->addMembersToDomainDeviceGroup($atoken, $gid, $device_ids);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AddMembersSToDomainDeviceGroupApi->addMembersToDomainDeviceGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **gid** | **int**| Group Id |
 **device_ids** | **string[]**| Array of device IDs |

### Return type

[**\Swagger\Client\Model\Success**](../Model/Success.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

