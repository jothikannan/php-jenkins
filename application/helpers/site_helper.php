<?php

function fb_pr($pdata)
{
	echo "<pre>";
	print_r($pdata);
	echo "</pre>";
}

function fb_generate_pagination($params = array()){
	$ci =& get_instance();
	$ci->load->library('pagination');
	
	/* This Application Must Be Used With BootStrap 3 *  */
	$config['full_tag_open'] = "<ul class='pagination'>";
	$config['full_tag_close'] ="</ul>";
	$config['num_tag_open'] = '<li class="paginate_button page-item">';
	$config['num_tag_close'] = '</li>';
	$config['cur_tag_open'] = "<li class='paginate_button page-item active'><a href='#'>";
	$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
	$config['next_tag_open'] = "<li class=\"paginate_button page-item next\">";
	$config['next_tag_close'] = "</li>";
	$config['prev_tag_open'] = "<li class=\"paginate_button page-item previous\">";
	$config['prev_tag_close'] = "</li>";
	$config['first_tag_open'] = "<li class=\"paginate_button page-item\">";
	$config['first_tag_close'] = "</li>";
	$config['last_tag_open'] = "<li class=\"paginate_button page-item\">";
	$config['last_tag_close'] = "</li>";
	
	//$config['base_url'] = site_url('/home/index');
	$config['base_url'] = $params['page_burl'];
	$config['total_rows'] = $params['total_rows'];
	$config['per_page'] = $params['per_page'];
	$config['use_page_numbers'] = TRUE;
	$config['reuse_query_string'] = TRUE;
	$config['uri_segment'] = $params['uri_segment'];

	$ci->pagination->initialize($config);

	$plinks = $ci->pagination->create_links();
	return $plinks;
}

function fb_combo_arr($table_name = "ponds1", $no_id = false){
	$ci =& get_instance();
	$msg = $ci->fb_rest->combo_list($table_name, $no_id);
	$cmb_list = array();
	if($msg["status"] == "success"){
		$cmb_list = $msg["combo_list"];
	}
	return $cmb_list;
}

function fb_convert_time($date){
	$time = strtotime($date);
	$time = $time;
	return $time;
}

function fb_convert_date($time){
	$date = date("m/d/Y", $time);
	return $date;
}

function fb_text($key){
	$ci =& get_instance();
	$txt = $ci->lang->line($key);
	return $txt;
}

function fb_lang_menu(){
	$ci =& get_instance();
	$ci->load->library('parser');
	$clang = $ci->config->item('language', 'fb_boodskap');
	$lang = $ci->fb_rest->get_fbuser_data('lang');
	$lang = (! empty($lang) ) ? $lang : "en";
	$cur_lang = $clang[$lang];
	$plang = array();
	$plang["cur_lang"] = $cur_lang["disp_txt"];
	$plang["cur_lang_icon"] = $cur_lang["icon_cls"];
	$lang_items = array();
	foreach($clang as $k => $alang){
		if ($lang == $k )
		  continue;
	    $curi_str = uri_string();
		$lang_url = site_url("common/change_lang?lang=".$k."&ruri=".$curi_str);
		$lang_text = $alang["disp_txt"];
		$lang_icon = $alang["icon_cls"];
		$lang_items[] = array("lang_url" => $lang_url, "lang_text" => $lang_text, "lang_icon" => $lang_icon);
	}
	$plang["lang_items"] = $lang_items;
	
	$ci->parser->set_delimiters("{", "}");
	$slang = $ci->parser->parse('include/lang_menu', $plang, true);
	return $slang;
	
}

function get_marketWeight($species_type){
	$ci =& get_instance();
	$table_name="species";
	$aresult = $ci->fb_rest->search_list($table_name, $species_type);
	if($aresult["status"] == "success"){
		  $cresult = $aresult["result_set"];
		  $crow = $cresult[0];
		  $csrc = $crow["_source"];
		  if(!empty($cresult)){
			  if($csrc["max_weight"]){
			 	 $marketWeight = $csrc["max_weight"];
			  }else{
				 $marketWeight = 0;
			  }
			  
		  }
	}
	return $marketWeight;
}

function get_sampling($pond){
	$ci =& get_instance();
	$table_name="sampling";
	$aresult = $ci->fb_rest->search_list($table_name, $pond);
	//print_r($aresult);
	$cresult = isset($aresult["result_set"]) ? $aresult["result_set"]: array();
	if($aresult["status"] == "success" && !empty($cresult[0])){
		
		$crow = $cresult[0];
		$csrc = $crow["_source"];
		 if(!empty($cresult) & $csrc["sampling_state"]=="true"){
			$avg_weight = 	$csrc["avg_weight"];	 
		 }else{
			 $avg_weight = 0;
		 }
	}else{
		$avg_weight = 0;
	}
			
	return $avg_weight;
}


function fb_cur_lang(){
	$ci =& get_instance();
	$clang = $ci->config->item('language', 'fb_boodskap');
	$lang = $ci->fb_rest->get_fbuser_data('lang');
	$lang = (! empty($lang) ) ? $lang : "en";
	$alang = $clang[$lang];
	return $alang;
}

function fb_jslang(){
	$ci =& get_instance();
	$alang = fb_cur_lang();
	//js_text_lang
	$lang = $ci->lang->load('js_text', $alang["lang_name"], true);
	return $lang;
}

function fb_message($type, $msg){
	$ci =& get_instance();
	$ci->load->library('parser');
	$params = array("message" => $msg);
	return $ci->parser->parse('message/'.$type, $params, true);
}

function fb_common_js(){
	$ci =& get_instance();
	$ci->load->library('parser');
	$params["site_url"] = site_url("/");
	$ajs_lang = fb_jslang();
	$params["site_err_lang"] = json_encode($ajs_lang);
	$cjs = $ci->parser->parse('include/common_js', $params, true);
	return $cjs;
	//$file = "common.js";
	//file_put_contents(FCPATH."assets/js/$file",utf8_encode($cjs),LOCK_EX);
}

function fb_sdate_qstr($dstr, $table_name){
	$ci =& get_instance();
	$ci->config->load('fb_boodskap', TRUE);
	$asearch_date = $ci->config->item('asearch_date', 'fb_boodskap');
	$srch_fld = $asearch_date[$table_name];
	$dtime = fb_convert_time($dstr);
	$edstr = date("m/d/Y", $dtime);
	$stime = $dtime;
	$etime = $dtime + ((24 * 60 * 60) - 1 );
	$lowv = $dtime;
	$topv = $etime;
	$range_fld = $srch_fld;
	return compact("lowv", "topv", "range_fld");
}

function fb_chk_date($dstr){
	$adate = explode("/",$dstr);
	if( count($adate) == 3)
	{
		$m = isset($adate[0]) ? $adate[0] : "";
		$d = isset($adate[1]) ? $adate[1] : "";
		$y = isset($adate[2]) ? $adate[2] : "";

		$m = (int) $m;
		$d = (int) $d;
		$y = (int) $y;
	
		$dflag = checkdate($m, $d, $y);
		return $dflag;
	} else {
		return false;
	}
}

function disp_weight($tcnt, $puwt){
	if($tcnt <= 0){
		return "-";
	}else{
		$twt = ($tcnt * $puwt);
		$twt = round($twt,2);
		return $twt;
	}
}

